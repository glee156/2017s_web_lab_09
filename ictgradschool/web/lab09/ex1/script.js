/**
 * Created by glee156 on 7/12/2017.
 */

$( function() {


    $( "#size-control" ).slider({
        min: 1,
        max: 10,
        value: 5,
        slide: function () {
            var selection = $( "#size-control" ).slider( "value" );
            console.log(selection);
            $("input[type='checkbox']:checked").each(function () {
                console.log(this);
                var dolphin = this.id.split("-")[1];
                $("#"+dolphin).attr("width", 50*selection);
            })
        }
    });
} );


$('[name="radio"]').click(function() {
   //console.log(this);
   if(this.id == "radio-background1"){
       $("#background").attr("src", "../images/background1.jpg")
   }
    else if(this.id == "radio-background2"){
        $("#background").attr("src", "../images/background2.jpg")
    }
   else if(this.id == "radio-background3"){
       $("#background").attr("src", "../images/background3.jpg")
   }
   else if(this.id == "radio-background4"){
       $("#background").attr("src", "../images/background4.jpg")
   }
   else if(this.id == "radio-background5"){
       $("#background").attr("src", "../images/background5.jpg")
   }
   else if(this.id == "radio-background6"){
       $("#background").attr("src", "../images/background6.gif")
   }

});

$("#toggle-components").change(function () {
    console.log("changed");
   $("input[type='checkbox']:checked").each(function () {
       console.log(this.id.split("-")[1]);
       var dolphin = this.id.split("-")[1];
       $("#"+dolphin).show();

   })

    $("input[type='checkbox']:not(:checked)").each(function () {
        console.log(this.id.split("-")[1]);
        var dolphin = this.id.split("-")[1];
        $("#"+dolphin).hide();

    })
});